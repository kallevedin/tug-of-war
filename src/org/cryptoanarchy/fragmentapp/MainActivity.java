package org.cryptoanarchy.fragmentapp;

import org.cryptoanarchy.fragmentapp.IrcWorker.IrcWorkerListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends Activity implements IrcWorkerListener {

    private MenuFragment connFrag;
    private IrcWorker ircWorker;

	@Override
	protected void onCreate(Bundle savedBundle) {
		super.onCreate(savedBundle);
		setContentView(R.layout.main_activity);


		if (savedBundle == null) {
	        connFrag = new MenuFragment();
	        
			getFragmentManager().beginTransaction()
				.add(R.id.container, connFrag, 
			    getString(R.string.main_fragment_tag)).commit();
		} else {
            connFrag = (MenuFragment) getFragmentManager()
                .findFragmentByTag(getString(R.string.main_fragment_tag));
		}
	}
	
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Must call this after fragment has been set up.
        connFrag.setStatus(getString(R.string.not_connected_msg));
    }

    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * IrcWorker callbacks 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    @Override
    public void onClose(String errorMsg) {
        String status = getString(R.string.disconnected_msg);
        if (errorMsg != null) {
           status += "\n" + errorMsg;
        }
        connFrag.setStatus(status);
        connFrag.setConnectActive(true);
    }

    @Override
    public void onOpponentMessage() {
        connFrag.setStatus("Message from opponent"); // TODO
    }

    @Override
    public void onConnect() {
        connFrag.setStatus(getString(R.string.connected_msg));
    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void playButtonClicked(View view) {
        startActivity(new Intent(this, GameActivity.class));
        finish(); // TODO Why was this here?
	}
	
    public void onConnectButton(View view) {
        connFrag.setConnectActive(false);
        ircWorker = new IrcWorker(connFrag.getPlayerName(), this);
        ircWorker.lauch();
        connFrag.setStatus(getString(R.string.connecting_msg));
    }

}
