package org.cryptoanarchy.fragmentapp;

public class TugGame {
    private float playerDistance = 0;
    private float finishDistance = 1_000; 
        
    public void pull(float dist) {
        playerDistance += dist;
    }

	public float getDistance() {
        return playerDistance;
    }
	
	public float getFinishDistance() {
		return finishDistance;
	}
}
