package org.cryptoanarchy.fragmentapp;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

public class LoggingGesturListener 
    implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener  {
    
    String logTag;
    
    public LoggingGesturListener(String logTag) {
        this.logTag = logTag;
    }

    private int logTouch(String action, MotionEvent... events) {
        return Log.d(logTag, action); // + Arrays.toString(events));
    }
    
    public boolean onDown(MotionEvent event) { 
        logTouch("onDown", event); 
        return true;
    }

    public boolean onFling(MotionEvent event1, MotionEvent event2, 
            float velocityX, float velocityY) {
        logTouch( "onFling", event1, event2);
        return true;
    }

    public void onLongPress(MotionEvent event) {
        logTouch("onLongPress", event); 
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
            float distanceY) {
        logTouch("onScroll", e1, e2);
        return true;
    }

    public void onShowPress(MotionEvent event) {
        logTouch("onShowPress", event);
    }

    public boolean onSingleTapUp(MotionEvent event) {
        logTouch("onSingleTapUp", event);
        return true;
    }

    public boolean onDoubleTap(MotionEvent event) {
        logTouch("onDoubleTap", event);
        return true;
    }

    public boolean onDoubleTapEvent(MotionEvent event) {
        logTouch("onDoubleTapEvent", event);
        return true;
    }

    public boolean onSingleTapConfirmed(MotionEvent event) {
        logTouch("onSingleTapConfirmed", event);
        return true;
    }
}