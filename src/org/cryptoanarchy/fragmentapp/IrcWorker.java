package org.cryptoanarchy.fragmentapp;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.Scanner;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;

public class IrcWorker implements Callback {
    
    public interface IrcWorkerListener {
        void onClose(String errorMsg);
        void onOpponentMessage();
        void onConnect();
    }
    
    public static final int 
        CONNECTED_MSG = 1,
        DISCONNECT_MSG = 2,
        ERROR_MSG = 3,
        OPPONENT_MSG = 4,

        DISCONNECT_NORMAL = 1,
        DISCONNECT_ERROR = 2;

    private static String LOG_TAG = "Android_worker";

    private volatile boolean stopRequested = false;
    private String playerName;
    private Handler msgHandler;
    private IrcWorkerListener listener;
    
    public IrcWorker(String playerName, IrcWorkerListener listener) {
        this.playerName = playerName;
        this.listener = listener;
    }
    
    @Override
    public boolean handleMessage(Message msg) {
        if (msg.what == IrcWorker.CONNECTED_MSG) {
            listener.onConnect();
        } else if (msg.what == IrcWorker.DISCONNECT_MSG) {
            String errorMsg = 
                msg.arg1 == IrcWorker.DISCONNECT_ERROR
                ? (String) msg.obj
                : null;
            listener.onClose(errorMsg);
        } else if (msg.what == IrcWorker.OPPONENT_MSG) {
            listener.onOpponentMessage();
        } else {
            throw new RuntimeException("Unexpected message from IRC worker: " + msg.what);    
        }
        return true;
    }
    

    public void lauch() {
        Log.d(LOG_TAG, "Connecting");
        msgHandler = new Handler(this);

        Thread ircThread = new Thread(new Runnable() {
                public void run() {
                    runIrc();
                }
            });
        ircThread.setName("IRC worker");
        ircThread.start();
    }
    
    public void requestClose() {
        stopRequested = true;
    }  
    
    private static void close(Closeable obj) {
        if (obj != null) try {
            obj.close();
        } catch (IOException exc) {
            Log.w(LOG_TAG, "Exception while closing IRC connection", exc);
        }
    }
    
    public class IrcException extends IOException {
        public IrcException(String detailMessage) {
            super(detailMessage);
        }
    }
    
    
    private void runIrc() {
        Socket socket = null;
        Scanner scan = null;
        
        try {
            socket = new Socket("gbgftw.jaywalk.se", 6667);
            socket.setTcpNoDelay(true);
            
            final InputStream inputStream = socket.getInputStream();
            final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            final Writer writer = new OutputStreamWriter(socket.getOutputStream());
            Log.d(LOG_TAG, Boolean.toString(socket.isConnected()));
            
            String connectStr = "USER tugofwar_bot 0 * :tugofwar_bot\r\nNICK " + playerName + "\r\n";
            Log.d(LOG_TAG, connectStr);
            writer.write(connectStr);
            writer.flush();
            
            
            while (!stopRequested) {
                String msg = reader.readLine();
                Log.d(LOG_TAG, "IRC message: " + msg);
                
                if (msg == null) {
                    Log.w(LOG_TAG, "Reader gave null");
                    stopRequested = true;
                    break;
                }
                
                scan = new Scanner(msg);
                
                scan.useDelimiter(" ");
                
                String first = scan.next();
                // String[] parts = msg.split(" ");
                
                if (first.equals("PING")) {
                    Log.d(LOG_TAG, "Heartbeat");
                    writer.write("PONG " + scan.next().substring(1) + "\r\n");
                    writer.flush();
                } else if (first.equals("ERROR")) {
                    Log.w(LOG_TAG, "IRC error: " + msg);
                    throw new IrcException(msg);
                } else if (first.charAt(0) == ':') {
                    String type = scan.next();
                    if (type.equals("376")) {
                        msgHandler.obtainMessage(CONNECTED_MSG).sendToTarget();
                    } else if (type.equals("PRIVMSG")) {
                        String otherNick = first.substring(1).substring(0, first.indexOf('!') - 1);
                        if (!scan.next().equals(playerName)) throw new IOException("Unexpected nick in PRIVMSG");
                        
                        msgHandler.obtainMessage(OPPONENT_MSG).sendToTarget();
                        String privMsg = scan.next().substring(1);
                        String echoMsg = "PRIVMSG " + otherNick + " :Echo: " + privMsg + "\r\n";
                        writer.write(echoMsg);
                        writer.flush();
                    }
                } else {
                 ; // Ignore all other messages.
                }
            }
            
            msgHandler.obtainMessage(DISCONNECT_MSG, DISCONNECT_NORMAL, -1).sendToTarget();
            
        } catch (IOException exc) {
            msgHandler.obtainMessage(DISCONNECT_MSG, DISCONNECT_ERROR, -1, exc.getMessage()).sendToTarget();
            Log.d(LOG_TAG, "IRC IO Exception" + exc.toString());
        } finally {
            close(socket);
            close(scan); // Just to get rid of resource leak warning.
        }
    }

}
