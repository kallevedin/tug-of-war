package org.cryptoanarchy.fragmentapp;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MenuFragment extends Fragment {
    
    @FindInView(R.id.status_view)
    private TextView statusView;

    @FindInView(R.id.player_name)
    private EditText playerNameText;
    
    @FindInView(R.id.connect_button)
    private Button connectButton;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_fragment, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AndroidUtils.loadViewsFromGui(this, getActivity());
    }

    public MenuFragment() {}
    
    public void setStatus(String msg) {
        statusView.setText(getString(R.string.status_prefix) + " " + msg);
    }
    
    public void setConnectActive(boolean active) {
        connectButton.setActivated(active);
    }
    
    public String getPlayerName() {
        return playerNameText.getText().toString();
    }
    
}