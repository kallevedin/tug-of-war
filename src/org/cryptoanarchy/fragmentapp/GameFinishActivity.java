package org.cryptoanarchy.fragmentapp;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class GameFinishActivity extends Activity {

	public static final String DID_I_WIN = "DID I WIN LOL";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_endscreen);
		
		boolean didIWin = getIntent().getBooleanExtra(DID_I_WIN, false);
		Log.d("GAME", "GameFinishActivity STARTED (win="+didIWin+")");
		
		if (savedInstanceState == null) {			
			Fragment endScreenFrag = new EndScreenFragment().setWin(didIWin);			
			getFragmentManager().beginTransaction().add(R.id.container, endScreenFrag).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_finish, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * When the user clicks on the "Play again!"-button, this happens
	 * @param view
	 */
	public void buttonNewGame(View view) {
		Intent i = new Intent(this, GameActivity.class);
		startActivity(i);
		finish();
	}
	
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class EndScreenFragment extends Fragment {

		private boolean youWon = false;
		
		public EndScreenFragment() {
		}
		
		public GameFinishActivity.EndScreenFragment setWin(boolean didIWin) {
			this.youWon = didIWin;
			return this;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			
			View rootView;
			if(youWon)
				rootView = inflater.inflate(R.layout.fragment_game_youwon, container, false);
			else
				rootView = inflater.inflate(R.layout.fragment_game_youlost, container, false);

			/*
			TextView tv = new TextView(getActivity());
			if( getActivity().getIntent().getBooleanExtra(DID_I_WIN, false) ){
				tv.setText(R.string.you_won_message);
				Log.d("GAME", "win message set");
			} else {
				tv.setText(R.string.you_failed_message);
				Log.d("GAME", "fail message set");
			}
			tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			//tv.setTextSize(20);
			tv.setTextColor(getActivity().getResources().getColor(R.color.black));
			container.addView(tv);
			*/
			
			return rootView;
		}
	}

}
