package org.cryptoanarchy.fragmentapp;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class GameActivity extends Activity {
    private static final String LOG_TAG = "Game";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                .add(R.id.container, new GameFragment())
                .commit();
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.game, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (item.getItemId() == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public static class GameFragment extends Fragment {
        private GestureDetector gestDet;
        
        private TugGame game = new TugGame();
        private ImageView ropeView;
        
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.game_fragment, container, false);
            
            ropeView = (ImageView) rootView.findViewById(R.id.rope_view);

            gestDet = new GestureDetector(getActivity(), new RopeGestureListener());
            // gestDet = new GestureDetector(getActivity(), new LoggingGesturListener(LOG_TAG));
            
            rootView.setOnTouchListener(new OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    gestDet.onTouchEvent(event);
                    // Log.d(LOG_TAG, MotionEvent.actionToString(event.getAction()));
                    return true;
                }
            });
            return rootView;
        }
        
        
        public class RopeGestureListener extends GestureDetector.SimpleOnGestureListener {
    
        	private boolean disabled = false;
        	
            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            	if(disabled) return false;
            	
                if (ropeView.getLeft() < e2.getX() && e2.getX() < ropeView.getRight()
                  && ropeView.getTop() < e2.getY() && e2.getY() < ropeView.getBottom()) {
                    game.pull(-distanceY);
                    Log.d(LOG_TAG, "pull: " + game.getDistance());
                } else {
                    Log.d(LOG_TAG, "outside");
                }
                
                if( game.getDistance() >= game.getFinishDistance() ) {
                	this.disabled = true;
                	Log.d(LOG_TAG, "YOUR WON MESSAGE SHOULD BE DISPLAYED NAO");
                	Intent i = new Intent(getActivity(), GameFinishActivity.class);
                	i.putExtra(GameFinishActivity.DID_I_WIN, true);
                	startActivity(i);
                	getActivity().finish();
                }
                
                return true;
            }
            
            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }
            
        }
    }
    
}


