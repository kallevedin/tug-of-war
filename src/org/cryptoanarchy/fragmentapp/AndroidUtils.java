package org.cryptoanarchy.fragmentapp;

import java.lang.reflect.Field;

import android.app.Activity;

public class AndroidUtils {
    /**
     * Equivalent to loadFromGui(o, o).
     */
    public static void loadViewsFromGui(Activity act) {
        loadViewsFromGui(act, act);
    }
    
    /**
     * In object o, set all fields which have a LoadFromGui annotation on them using
     * the findViewById method on act.
     */
    public static void loadViewsFromGui(Object o, Activity act) {
        Class<?> cls = o.getClass();
        try {
            Field[] fs = cls.getDeclaredFields();
            for (Field f : fs) {
                FindInView ann = f.getAnnotation(FindInView.class);
                if (ann != null) {
                    f.setAccessible(true);
                    Object v = act.findViewById(ann.value());
                    
                    if (v == null) {
                        throw new IllegalStateException("Loading view that is not found: " + f.getName());
                    }
                    Class<?> fieldType = f.getType();
                    if (!fieldType.isInstance(v)) {
                        throw new IllegalStateException(
                            "Field not assignable from view type. Field type: "
                            + fieldType.getName() + ", View type: " + v.getClass().getName());
                    }
                    
                    f.set(o, v);
                    
                    f.setAccessible(false);
                }
            }
        } catch (IllegalAccessException exc) {
            throw new RuntimeException( exc );
        } catch (IllegalArgumentException exc) {
            throw new RuntimeException( exc );
        }
            
    }
}
